﻿using System;

namespace System.Net {
    public static class HttpStatusCodeExtensions {

        public static int ToInt32(this System.Net.HttpStatusCode statusCode) {
            return (int)statusCode;
        }

    }
}
