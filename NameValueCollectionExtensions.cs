﻿using System;
using System.Collections;
using System.Collections.Specialized;

namespace System.Collections {

    public static class NameValueCollectionExtensions {
        public static bool ContainsKey(this NameValueCollection coll, string key) {
            if(coll.IsNullOrEmpty() || key.IsNullOrEmpty()) {
                return false;
            }

            foreach(string testKey in coll.AllKeys) {
                if(testKey.Equals(key)) {
                    return true;
                }
            }

            return false;
        }
    }
}
