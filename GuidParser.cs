using System;
using System.Collections.Generic;
using System.Text;

namespace System
{
    public class GuidParser
    {

        public static string GuidRegex = @"(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}";

        private static System.Text.RegularExpressions.Regex isGuid = new System.Text.RegularExpressions.Regex("^" + GuidRegex + "$", System.Text.RegularExpressions.RegexOptions.Compiled);

        public static bool TryParseGuid(string candidate, System.Text.RegularExpressions.Regex regex, out Guid output)
        {
            output = Guid.Empty;

            if (!String.IsNullOrEmpty(candidate) && regex != null)
            {
                System.Text.RegularExpressions.Match m = regex.Match(candidate);
                if (m != null && m.Success)
                {
                    output = new Guid(m.Groups["guid"].Value);
                    return true;
                }
            }

            return false;
        }

        public static bool TryParseGuid(string candidate, out Guid output)
        {

            bool isValid = false;
            output = Guid.Empty;

            if (candidate != null)
            {

                if (isGuid.IsMatch(candidate))
                {
                    output = new Guid(candidate);
                    isValid = true;
                }
            }

            return isValid;

        }

        public static bool TryParseGuid(string candidate, out Guid? output)
        {

            bool isValid = false;
            output = null;

            if (candidate != null)
            {

                if (isGuid.IsMatch(candidate))
                {
                    output = new Guid(candidate);
                    isValid = true;
                }
            }

            return isValid;

        }

        public static bool IsGuid(string candidate)
        {
            if(!String.IsNullOrEmpty(candidate))
            {
                return GuidParser.isGuid.IsMatch(candidate);
            }
            return false;
        }
 
    }
}
