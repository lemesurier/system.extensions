﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Configuration {
    public static class ConnectionStrings {

        private static ConnectionStringSettings Get(IEnumerable<string> names) {
            if (null == names) return null;
            return names.Select(name => ConfigurationManager.ConnectionStrings[name]).FirstOrDefault(conn => null != conn) ?? null;
        }

        public static ConnectionStringSettings Get(string name) {

            if(name.IsNullOrWhiteSpace()) return null;

            var list = new List<string> { name };
            list.AddRange(name.Split(new [] { '.' }, StringSplitOptions.RemoveEmptyEntries));

            return Get(list);// ?? new ConnectionStringSettings(name, name);// ?? GetConnectionString();

        }
    }
}
