﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Collections.Generic
{
	public static class GenericICollectionExtensions
	{
		/// <summary>
		/// Indicates whether <paramref name="collection"/> contains any of the members in <paramref name="toFind"/>.
		/// </summary>
		public static bool ContainsAny<T>(this ICollection<T> collection, ICollection<T> toFind)
		{
			var found = false;

			foreach (var item in toFind)
			{
				if (collection.Contains(item))
				{
					found = true;
					break;
				}
			}

			return found;
		}

		/// <summary>
		/// Determines whether <paramref name="collection"/> contains all of the members of the <paramref name="toFind"/> collection.
		/// </summary>
		public static bool ContainsAll<T>(this ICollection<T> collection, ICollection<T> toFind)
		{
			bool foundAll;

			if (toFind.Count == 0)
			{
				foundAll = false;
			}
			else
			{
				foundAll = true;

				foreach (T item in toFind)
				{
					if (!collection.Contains(item))
					{
						foundAll = false;
						break;
					}
				}

			}
			return foundAll;
		}

		/// <summary>
		/// Performs the specified action on each element of the <paramref name="collection"/>.
		/// </summary>
		public static void ForEach<T>(this ICollection<T> collection, Action<T> action)
		{
			foreach (T item in collection)
			{
				action(item);
			}
		}

		/// <summary>
		/// Creates an array of the items in <paramref name="collection"/>.
		/// </summary>
		public static T[] ToArray<T>(this ICollection collection)
		{
			T[] array = new T[collection.Count];
			int index = 0;

			foreach (T item in collection)
			{
				array[index++] = item;
			}

			return array;
		}

		/// <summary>
		/// Creates an array of type K, utilizing the <paramref name="converter"/> to convert from T.
		/// </summary>
		public static K[] ToArray<T, K>(this ICollection<T> collection, Converter<T, K> converter)
		{
			K[] array = new K[collection.Count];
			int index = 0;

			foreach (T item in collection)
			{
				array[index++] = converter(item);
			}

			return array;
		}

        public static bool IsNullOrEmpty<T>(this ICollection<T> list) {
            return list == null || list.Count == 0;
        }
	}
}
