﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System {
    public class Password {
        private static char[] chars = "ABCDEFGHJKLMNPQRSTUVWXYZ2345678".ToCharArray();
     
        public static string Random(int length = 8) {
            char[] pw = new char[length];
            Random rand = new Random();
            for (int i = 0; i < pw.Length; i++) {
                pw[i] = chars[rand.Next(0, chars.Length)];
            }
            return new String(pw);
        }
    }
}
