﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace System {
    public struct ShortGuid {
        private readonly Guid guid;

        public ShortGuid(Guid guid) {
            this.guid = guid;
        }

        public ShortGuid(string guid) {
            Guid g;
            TryParseGuid(guid, out g);
            this.guid = g;
        }

        //static Regex _invalid = new Regex(@"[^0-9a-zA-Z_\-]");


        public static bool TryParseGuid(string input, out Guid result) {
            Guid parsed;
            if (Guid.TryParse(input, out parsed)) {
                result = parsed;
                return true;
            }

            try {
                // keep an eye out for converted dashes
                parsed = new Guid(Convert.FromBase64String(input.Replace(" ", "").Replace("_", "/").Replace("‐", "-").Replace("-", "+") + "=="));
            } catch {
                result = parsed;
                return false;
            }

            result = parsed;
            return true;
        }

        public static bool TryParse(string input, out ShortGuid result) {

            Guid g;
            if (TryParseGuid(input, out g)) {
                result = new ShortGuid(g);
                return true;
            }

            result = new ShortGuid();
            return false;
        }

        public override string ToString() {
            return Convert.ToBase64String(guid.ToByteArray())
                .Substring(0, 22)
                .Replace("/", "_")
                .Replace("+", "-");
        }

        public Guid ToGuid() {
            return this.guid;
        }

        public static implicit operator string(ShortGuid guid) {
            return guid.ToString();
        }

        /*public static implicit operator ShortGuid(Guid guid) {
            return new ShortGuid(guid);
        }*/

        public static implicit operator Guid(ShortGuid shortGuid) {
            return shortGuid.guid;
        }
    }

    public static class ShortGuidExtensions {
        public static ShortGuid ToShortGuid(this Guid guid) {
            return new ShortGuid(guid);
        }
		public static ShortGuid? ToShortGuid(this Guid? guid) {
			if (null == guid) return null;
			return new ShortGuid((Guid) guid);
		}
        public static ShortGuid Short(this Guid guid) {
            return guid.ToShortGuid();
        }
		public static ShortGuid? Short(this Guid? guid) {
			if (null == guid) return null;
			return new ShortGuid((Guid)guid);
		}
    }
}