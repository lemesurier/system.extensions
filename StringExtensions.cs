﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;

namespace System
{
	public static class StringExtensions
	{
		/// <summary>
		/// Returns the default value if <paramref name="s"/> is null or empty.
		/// </summary>
		public static string Default(this string s, string defaultValue)
		{
			if (string.IsNullOrEmpty(s))
			{
				return defaultValue;
			}
			else
			{
				return s;
			}
		}

		/// <summary>
		/// Returns the default value if <paramref name="s"/> is null.
		/// </summary>
		public static string Default(this string s, string defaultValue, bool onlyWhenNull)
		{
			if (onlyWhenNull)
			{
				if (s == null)
				{
					return defaultValue;
				}
				else
				{
					return s;
				}
			}
			else
			{
				return Default(s, defaultValue);
			}
		}

		/// <summary>
		/// Creates list of non-null and empty values from the object passed to <paramref name="strings"/>.
		/// </summary>
		public static IEnumerable<string> RemoveEmptyElements(this IEnumerable<string> strings)
		{
			foreach (var s in strings)
			{
				if (!string.IsNullOrEmpty(s))
				{
					yield return s;
				}
			}
		}

		/// <summary>
		/// Same as String.Format, but with less typing.
		/// </summary>
        private static Dictionary<string, Regex> cache = new Dictionary<string, Regex>();
        private static Regex cacheRegex(string r) {
            if (!cache.ContainsKey(r))
                cache[r] = new Regex(r, RegexOptions.Compiled);
            return cache[r];
        }

        public static bool IsMatch(this string s, string regex) {
            return s.IsMatch(cacheRegex(regex));
        }

        public static MatchCollection Matches(this string s, string regex) {
            return s.Matches(cacheRegex(regex));
        }

        public static Match Match(this string s, string regex) {
            return s.Match(cacheRegex(regex));
        }

        public static string[] Split(this string s, string regex) {
            return s.Split(cacheRegex(regex));
        }

        public static string Replace(this string s, Regex r, string replacement) {
            return r.Replace(s, replacement);
        }

        public static bool IsMatch(this string s, Regex r) {
            return r.IsMatch(s);
        }

        public static MatchCollection Matches(this string s, Regex r) {
            return r.Matches(s);
        }

        public static Match Match(this string s, Regex r) {
            return r.Match(s);
        }

        public static string[] Split(this string s, Regex r) {
            return r.Split(s);
        }

        public static string Replace(this string s, string regex, string replacement) {
            Regex r = cacheRegex(regex);
            return r.Replace(s, replacement);
        }

        public static bool IsEmpty(this string str) {

            if (str != null && str.Length == 0) {
                return true;
            }

            return false;
        }


        public static bool IsNullOrEmpty(this string str) {
            return string.IsNullOrEmpty(str);
        }

        public static bool IsNullOrWhiteSpace(this string str) {
            return string.IsNullOrWhiteSpace(str);
        }

        // http://haacked.com/archive/2009/01/04/fun-with-named-formats-string-parsing-and-edge-cases.aspx#70316
        public static string FormatWith(this string format, object source)
        {
            return FormatWith(format, null, source);
        }

        private static string FormatWith(this string format, IFormatProvider provider, object source)
        {
            if (format == null)
                throw new ArgumentNullException("format");

            List<object> values = new List<object>();
            string rewrittenFormat = Regex.Replace(format,
                @"(?<start>\{)+(?<property>[\w\.\[\]]+)(?<format>:[^}]+)?(?<end>\})+",
                delegate(Match m)
                {
                    Group startGroup = m.Groups["start"];
                    Group propertyGroup = m.Groups["property"];
                    Group formatGroup = m.Groups["format"];
                    Group endGroup = m.Groups["end"];

                    values.Add((propertyGroup.Value == "0")
                      ? source
                      : System.Web.UI.DataBinder.Eval(source, propertyGroup.Value));

                    return new string('{', startGroup.Captures.Count) + (values.Count - 1) + formatGroup.Value
                      + new string('}', endGroup.Captures.Count);
                },
                RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

            return string.Format(provider, rewrittenFormat, values.ToArray());
        }

        public static string FormatWith(this string format, params object[] args) {

            if (format == null) {
                throw new ArgumentNullException("format");
            }



            return string.Format(format, args);

        }



        private static string FormatWith(this string format, IFormatProvider provider, params object[] args) {

            if (format == null) {
                throw new ArgumentNullException("format");
            }



            return string.Format(provider, format, args);

        }

        //public static string FormatWith(this string format, object source) {

        //    return FormatWith(format, null, source);

        //}

        //public static string FormatWith(this string format, IFormatProvider provider, object source) {

        //    if (format == null)

        //        throw new ArgumentNullException("format");



        //    Regex r = new Regex(@"(?<start>\{)+(?<property>[\w\.\[\]]+)(?<format>:[^}]+)?(?<end>\})+",

        //      RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);



        //    List<object> values = new List<object>();

        //    string rewrittenFormat = r.Replace(format, delegate(Match m) {

        //        Group startGroup = m.Groups["start"];

        //        Group propertyGroup = m.Groups["property"];

        //        Group formatGroup = m.Groups["format"];

        //        Group endGroup = m.Groups["end"];



        //        values.Add((propertyGroup.Value == "0")

        //          ? source

        //          : DataBinder.Eval(source, propertyGroup.Value));



        //        return new string('{', startGroup.Captures.Count) + (values.Count - 1) + formatGroup.Value

        //          + new string('}', endGroup.Captures.Count);

        //    });



        //    return string.Format(provider, rewrittenFormat, values.ToArray());

        //}

        private static System.Text.RegularExpressions.Regex _invalidChars = new System.Text.RegularExpressions.Regex(@"[^\w\-_]", System.Text.RegularExpressions.RegexOptions.Compiled);
        private static System.Text.RegularExpressions.Regex _spaceChars = new System.Text.RegularExpressions.Regex(@"[&=/\\\s]", System.Text.RegularExpressions.RegexOptions.Compiled);
        private static System.Text.RegularExpressions.Regex _multipleUnderscoreChars = new System.Text.RegularExpressions.Regex(@"_+", System.Text.RegularExpressions.RegexOptions.Compiled);
        private static System.Text.RegularExpressions.Regex _multipleDashChars = new System.Text.RegularExpressions.Regex(@"-+", System.Text.RegularExpressions.RegexOptions.Compiled);


        public static string ToUrlReady(this string s) {

            if (s.IsNullOrEmpty()) {
                return s;
            }

            string prepped = s.ToLower().Replace(_spaceChars, "-").Replace(_multipleUnderscoreChars, "-").Replace(_multipleDashChars, "-").Replace(_invalidChars, string.Empty);

            return prepped.Length > 100 ? prepped.Substring(0, 100) : prepped;


        }

        public static string EnsureEndsWith(this string s, string value) {

            if (value.IsNullOrEmpty()) {
                return s;
            }

            if (s == null) {
                return value;
            }

            if (s.EndsWith(value)) {
                return s;
            }

            return s += value;

        }

        public static string EnsureEndsWith(this string s, string value, StringComparison comparisonType) {

            if (value.IsNullOrEmpty()) {
                return s;
            }

            if (s == null) {
                return value;
            }

            if (s.EndsWith(value, comparisonType)) {
                return s;
            }

            return s += value;

        }

        public static bool Contains(this string @this, string value, StringComparison comparisonType) {
            if (@this == null) {
                throw new ArgumentNullException("this");
            }

            return @this.IndexOf(value, comparisonType) >= 0;
        }

        public static bool ContainsAny(this string @this, IEnumerable<string> value, StringComparison comparisonType) {
            if (@this == null) {
                throw new ArgumentNullException("this");
            }
            if (value == null) {
                throw new ArgumentNullException("value");
            }

            return value.Any(v => @this.Contains(v, comparisonType));
        }
	}
}
