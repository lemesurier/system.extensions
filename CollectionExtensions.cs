﻿using System;
using System.Collections;

namespace System.Collections {
    public static class CollectionExtensions {
        public static bool IsNullOrEmpty(this ICollection list) {
            return list == null || list.Count == 0;
        }
    }

    public static class IListExtensions {

        public static bool Contains(this IList list, object value) {
            if (list.IsNullOrEmpty()) {
                return false;
            }
            return list.IndexOf(value) >= 0;
        }
    }
}
