﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System {
    public static class DateTimeExtensions {

        public static DateTimeOffset TruncateMillisecond(this DateTimeOffset dateTimeOffset) {
            return new DateTimeOffset(dateTimeOffset.Year, dateTimeOffset.Month, dateTimeOffset.Day, dateTimeOffset.Hour, dateTimeOffset.Minute, dateTimeOffset.Second, dateTimeOffset.Offset);
        }

        public static DateTimeOffset ToLocalTime(this DateTime datetime, TimeZoneInfo tz, bool assumedUtc) {
            if (assumedUtc) return datetime.ToAssumedUtc().ToLocalTime(tz);
            return datetime.ToLocalTime(tz);
        }

        public static DateTimeOffset ToLocalTime(this DateTime datetime, TimeZoneInfo tz) {
            if (tz == null) return datetime;

            if (datetime.Kind == DateTimeKind.Local) datetime = datetime.ToUniversalTime(); // assume system local

            // if unspecified, assume utc



            DateTime local = TimeZoneInfo.ConvertTimeFromUtc(datetime, tz);
            return new DateTimeOffset(local, tz.GetUtcOffset(local));
        }

		public static DateTimeOffset ToLocalTime(this DateTimeOffset datetime, TimeZoneInfo tz) {
			if(tz == null) return datetime;

			DateTime local = TimeZoneInfo.ConvertTimeFromUtc(datetime.UtcDateTime, tz);
			return new DateTimeOffset(local, tz.GetUtcOffset(local));
		}

        public static DateTime ToUniversalTime(this DateTime datetime, TimeZoneInfo tz, bool assumedLocalToTZ) {
            if (assumedLocalToTZ) return new DateTime(datetime.Ticks, DateTimeKind.Unspecified).ToUniversalTime(tz); // assume local and convert to unspecified
            return datetime.ToUniversalTime(tz); // system defaults
        }

        public static DateTime ToUniversalTime(this DateTime datetime, TimeZoneInfo tz) {
            if (datetime.Kind == DateTimeKind.Utc) return datetime; // assume already utc
            if (datetime.Kind == DateTimeKind.Local) return datetime.ToUniversalTime();  // assume system local
            // if unspecified, assume local to tz
            if (tz == null) return datetime;

            return TimeZoneInfo.ConvertTimeToUtc(datetime, tz);

        }

        public static DateTime TruncateMillisecond(this DateTime dateTime) {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second, dateTime.Kind);
        }

        /// <summary>
        /// Assumes the DateTime represents a UTC DateTime and returns a new DateTimeOffset with zero offset
        /// </summary>
        public static DateTimeOffset ToAssumedUtcOffset(this DateTime utcDateTime) {
            return new DateTimeOffset(utcDateTime.Year, utcDateTime.Month, utcDateTime.Day, utcDateTime.Hour, utcDateTime.Minute, utcDateTime.Second, utcDateTime.Millisecond, TimeSpan.Zero);
        }

        public static DateTime ToAssumedUtc(this DateTime utcDateTime) {
            return new DateTime(utcDateTime.Year, utcDateTime.Month, utcDateTime.Day, utcDateTime.Hour, utcDateTime.Minute, utcDateTime.Second, utcDateTime.Millisecond, DateTimeKind.Utc);
        }

        private static string _iso8601format = "yyyy-MM-dd'T'HH:mm:ss.fff'Z'";
        private static string _iso8601formatNoFractional = "yyyy-MM-dd'T'HH:mm:ss'Z'";

        public static string ToISO8601(this DateTime datetime, bool fractionalSeconds = false) {
            return datetime.ToUniversalTime().ToString(fractionalSeconds ? _iso8601format : _iso8601formatNoFractional);
        }

        public static string ToISO8601(this DateTimeOffset datetime, bool fractionalSeconds = false, bool utc = true) {
            if(utc) return datetime.UtcDateTime.ToString(fractionalSeconds ? _iso8601format : _iso8601formatNoFractional);
            return datetime.ToString(fractionalSeconds ? "o" : "yyyy'-'MM'-'dd'T'HH':'mm':'sszzz");
        }
    }
}
