﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Common;
using System.Data;
using System.Web.Caching;

namespace System.Data {
    public abstract class DBClientBase {

        private System.Configuration.ConnectionStringSettings _connectionString;

        private DbProviderFactory _dbProviderFactory;



        public DBClientBase() {
            //this.ConnectionString = GetConnectionString();
        }



        public DBClientBase(System.Configuration.ConnectionStringSettings connectionString) {

            this.ConnectionString = connectionString;

        }

        /*private System.Configuration.ConnectionStringSettings GetConnectionString() {
            return System.Configuration.ConfigurationManager.ConnectionStrings[0];
        }

        private System.Configuration.ConnectionStringSettings GetConnectionString(IEnumerable<string> names) {
            if (null == names) return GetConnectionString();
            return names.Select(name => System.Configuration.ConfigurationManager.ConnectionStrings[name]).FirstOrDefault(conn => null != conn) ?? GetConnectionString();
        }*/

        public DBClientBase(string connectionStringName) {

            this.ConnectionString = System.Configuration.ConnectionStrings.Get(connectionStringName);

           

        }



        public System.Configuration.ConnectionStringSettings ConnectionString {

            get {

                return this._connectionString;

            }

            set {

                this._connectionString = value;

                this._dbProviderFactory = DbProviderFactories.GetFactory(this._connectionString.ProviderName);

            }

        }



        public DbProviderFactory DBProviderFactory {

            get {

                return this._dbProviderFactory;

            }

        }



        public DbConnection CreateConnection() {

            DbConnection conn = this.DBProviderFactory.CreateConnection();

            conn.ConnectionString = this.ConnectionString.ConnectionString;

            return conn;

        }



        public DbDataAdapter CreateDataAdapter() {

            return this.DBProviderFactory.CreateDataAdapter();

        }



        public DbDataAdapter CreateDataAdapter(string selectCommandText) {

            return this.CreateDataAdapter(this.CreateCommand(selectCommandText, this.CreateConnection()));

        }



        public System.Data.Common.DbDataAdapter CreateDataAdapter(DbCommand selectCommand) {

            DbDataAdapter ad = this.DBProviderFactory.CreateDataAdapter();

            ad.SelectCommand = selectCommand;

            return ad;

        }



        public DbCommand CreateCommand() {

            return this.CreateCommand(this.CreateConnection());

        }



        public DbCommand CreateCommand(string commandText) {

            return this.CreateCommand(commandText, this.CreateConnection());

        }



        public DbCommand CreateCommand(string commandText, CommandType type) {

            return this.CreateCommand(commandText, type, this.CreateConnection());

        }



        public DbCommand CreateCommand(string commandText, DbConnection connection) {

            DbCommand cmd = this.CreateCommand(connection);

            cmd.CommandText = commandText;

            return cmd;

        }



        public DbCommand CreateCommand(string commandText, CommandType type, DbConnection connection) {

            DbCommand cmd = this.CreateCommand(commandText, connection);

            //cmd.CommandText = commandText;

            cmd.CommandType = type;

            return cmd;

        }



        public System.Data.Common.DbCommand CreateCommand(DbConnection connection) {

            DbCommand cmd = this.DBProviderFactory.CreateCommand();

            if (connection != null) {

                cmd.Connection = connection;

            }



            return cmd;

        }

        public DbParameter CreateParameter(string name, DateTime value) {

            DbParameter p = this.DBProviderFactory.CreateParameter();

            p.ParameterName = name;

            p.DbType = DbType.DateTime;

            p.Value = value;

            return p;

        }

        public DbParameter CreateParameter(string name, Guid value) {

            DbParameter p = this.DBProviderFactory.CreateParameter();

            p.ParameterName = name;

            p.DbType = DbType.Guid;

            p.Value = value;

            return p;

        }

        public DbParameter CreateParameter(string name, Guid? value) {

            DbParameter p = this.DBProviderFactory.CreateParameter();

            p.ParameterName = name;

            p.DbType = DbType.Guid;

            if (value.HasValue) p.Value = value.Value;
            else p.Value = DBNull.Value;
            return p;

        }

        public DbParameter CreateParameter(string name, DateTime? value) {

            DbParameter p = this.DBProviderFactory.CreateParameter();

            p.ParameterName = name;

            p.DbType = DbType.DateTime;

            if (value.HasValue) p.Value = value.Value;
            else p.Value = DBNull.Value;
            return p;

        }

        public DbParameter CreateParameter(string name, string value) {

            DbParameter p = this.DBProviderFactory.CreateParameter();

            p.ParameterName = name;

            p.DbType = DbType.String;

            if (value != null) p.Value = value;
            else p.Value = DBNull.Value;
            return p;

        }

        public DbParameter CreateParameter(string name, int value) {

            DbParameter p = this.DBProviderFactory.CreateParameter();

            p.ParameterName = name;

            p.DbType = DbType.Int32;

            p.Value = value;

            return p;

        }

        public DbParameter CreateParameter(string name, int? value) {

            DbParameter p = this.DBProviderFactory.CreateParameter();

            p.ParameterName = name;

            p.DbType = DbType.Int32;

            if (value.HasValue) p.Value = value.Value;
            else p.Value = DBNull.Value;
            return p;

        }

        public DbParameter CreateParameter(string name, ulong value) {

            DbParameter p = this.DBProviderFactory.CreateParameter();

            p.ParameterName = name;

            p.DbType = DbType.Int64;

            p.Value = value;
            return p;

        }

        public DbParameter CreateParameter(string name, ulong? value) {

            DbParameter p = this.DBProviderFactory.CreateParameter();

            p.ParameterName = name;

            p.DbType = DbType.Int64;

            if (value.HasValue) p.Value = value.Value;
            else p.Value = DBNull.Value;
            return p;

        }

        public DbParameter CreateParameter(string name, decimal value) {

            DbParameter p = this.DBProviderFactory.CreateParameter();

            p.ParameterName = name;

            p.DbType = DbType.Decimal;

            p.Value = value;

            return p;

        }

        public DbParameter CreateParameter(string name, double value) {

            DbParameter p = this.DBProviderFactory.CreateParameter();

            p.ParameterName = name;

            p.DbType = DbType.Double;

            p.Value = value;

            return p;

        }

        public DbParameter CreateParameter(string name, bool value) {

            DbParameter p = this.DBProviderFactory.CreateParameter();

            p.ParameterName = name;

            p.DbType = DbType.Boolean;

            p.Value = value;

            return p;

        }

        public DbParameter CreateParameter(string name, bool? value) {

            DbParameter p = this.DBProviderFactory.CreateParameter();

            p.ParameterName = name;

            p.DbType = DbType.Boolean;

            if (value.HasValue) p.Value = value.Value;
            else p.Value = DBNull.Value;
            return p;

        }

        public DbParameter CreateParameter(string name, object value, DbType type) {

            DbParameter p = this.DBProviderFactory.CreateParameter();

            p.ParameterName = name;

            p.DbType = type;

            if (value == null) {

                p.Value = DBNull.Value;

            } else {

                p.Value = value;

            }

            return p;

        }

        public DbParameter CreateParameter(string name, double? value, DbType type) {

            DbParameter p = this.DBProviderFactory.CreateParameter();

            p.ParameterName = name;
            p.DbType = type;
            if (value.HasValue) p.Value = value.Value;
            else p.Value = DBNull.Value;
            return p;

        }

        public DbParameter CreateParameter(string name, object value, DbType type, int size) {

            DbParameter p = this.CreateParameter(name, value, type);

            p.Size = size;

            return p;

        }



        public DbParameter CreateParameter(string name, object value, DbType type, ParameterDirection direction) {

            DbParameter p = this.CreateParameter(name, value, type);

            p.Direction = direction;

            return p;

        }





        public DbCommandBuilder CreateCommandBuilder(DbDataAdapter adapter) {

            DbCommandBuilder cmdBuilder = this.DBProviderFactory.CreateCommandBuilder();

            cmdBuilder.DataAdapter = adapter;

            return cmdBuilder;

        }



        public DataTable GetDataTable(DbCommand command) {

            DbDataAdapter a = this.CreateDataAdapter(command);

            DataTable table = new DataTable();

            a.Fill(table);

            return table;

        }



        public DataSet GetDataSet(DbCommand command) {

            DbDataAdapter a = this.CreateDataAdapter(command);

            DataSet set = new DataSet();

            a.Fill(set);

            return set;

        }



        public DataTable GetDataTable(string commandText, IEnumerable<DbParameter> parameters) {



            DataTable dt = new DataTable();



            using (DbConnection conn = this.CreateConnection()) {

                using (DbCommand cmd = this.CreateCommand(commandText, CommandType.Text, conn)) {

                    foreach (DbParameter p in parameters) {

                        cmd.Parameters.Add(p);

                    }

                    using (DbDataAdapter adapter = this.CreateDataAdapter(cmd)) {

                        adapter.Fill(dt);

                    }

                }

            }



            return dt;

        }



        public DataTable GetDataTable(string commandText, DbParameter parameter) {



            return GetDataTable(commandText, new List<DbParameter> { parameter });

        }



        public DataTable GetDataTable(string commandText, string parameterName, object parameterValue, DbType parameterType) {



            return GetDataTable(commandText, new List<DbParameter> { this.CreateParameter(parameterName, parameterValue, parameterType) });

        }

        /*
        public SqlCacheDependency GetCacheDependency(string table) {
            return new SqlCacheDependency(this.ConnectionString.Name, table);
        }

        public SqlCacheDependency GetCacheDependency(string table, string field) {
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(this.ConnectionString.ConnectionString)) {
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SELECT {1} FROM {0}".FormatWith(table, field), conn)) {
    
                    SqlCacheDependency dependency = new SqlCacheDependency(cmd);

                    cmd.Connection.Open();
                    //using(System.Data.SqlClient.SqlDataReader r = cmd.ExecuteReader()) r.Close();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();
                    //cmd.ExecuteNonQuery();

                    return dependency;
                }
            }
        }

        private SqlCacheDependency GetCacheDependency(string table, string field, Guid value) {
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(this.ConnectionString.ConnectionString)) {
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SELECT {0} FROM [dbo].[{1}] WHERE [{2}] = @ID".FormatWith(TableFieldMap["[{0}]".FormatWith(table)], table, field), conn)) {
                    cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ID", SqlDbType.UniqueIdentifier) { Value = value });
                    SqlCacheDependency dependency = new SqlCacheDependency(cmd);

                    cmd.Connection.Open();
                    //using(System.Data.SqlClient.SqlDataReader r = cmd.ExecuteReader()) r.Close();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();
                    //cmd.ExecuteNonQuery();
                    return dependency;
                }
            }
        }*/
    }
}