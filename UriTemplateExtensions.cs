﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System {
    public static class UriTemplateExtensions {

        /*public static Uri BindByName(this UriTemplate template, Uri baseAddress, IEnumerable<KeyValuePair<string, object>> parameters) {

            System.Collections.Specialized.NameValueCollection params2 = new System.Collections.Specialized.NameValueCollection();

            if (parameters != null) {
                foreach(KeyValuePair<string, object> pair in parameters) {
                    params2.Add(pair.Key, pair.Value.ToString());
                }
            }

            return template.BindByName(baseAddress, params2);

        }

        public static Uri BindByName(this UriTemplate template, Uri baseAddress, IEnumerable<KeyValuePair<string, string>> parameters) {
            System.Collections.Specialized.NameValueCollection params2 = new System.Collections.Specialized.NameValueCollection();

            if (parameters != null) {
                foreach (KeyValuePair<string, string> pair in parameters) {
                    params2.Add(pair.Key, pair.Value);
                }
            }

            return template.BindByName(baseAddress, params2);
        }*/

        public static bool ContainsWildcard(this UriTemplate template) {

            if (template != null) {

                return template.ToString().Contains('*');
            }


            return false;

        }
    }
}
