﻿using System;
using System.Collections.Generic;

namespace System.Collections.Generic
{
	// Required because compiler complains that both "Find"s have the same signature when
	// they're in the same class.
	public static class GenericIEnumerableExtensions
	{
		/// <summary>
		/// Searches a collection for an item.
		/// </summary>
		/// <returns>The found value, or the default value for "T".</returns>
		public static T Find<T>(this IEnumerable<T> collection, Predicate<T> condition)
		{
			T foundItem = default(T);

			foreach (T item in collection)
			{
				if (condition(item))
				{
					foundItem = item;
					break;
				}
			}

			return foundItem;
		}

        public static IEnumerable<K> Convert<T, K>(this IEnumerable<T> collection, Converter<T, K> converter) {
            List<K> list = new List<K>();

            foreach (T item in collection) {
                list.Add(converter(item));
            }

            return list;
        }

        public static void ForEach<T>(this IEnumerable<T> enumeration, Action<T> action) {
            foreach (T item in enumeration) {
                action(item);
            }
        }
	}
}