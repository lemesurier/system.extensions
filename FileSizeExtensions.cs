﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System {

    public static class Int16FileSizeExtensions {
        public static string ToFileSizeString(this Int16 bytes) {

            return ((long)bytes).ToFileSizeString();
        }

    }

    public static class Int32FileSizeExtensions {
        public static string ToFileSizeString(this Int32 bytes) {
            return ((long)bytes).ToFileSizeString();
        }
    }

    public static class Int64FileSizeExtensions {

        public static string ToFileSizeString(this Int64 bytes) {

            double s = bytes;
            string[] format = new string[] { "{0} bytes", "{0} KB", "{0} MB", "{0} GB", "{0} TB", "{0} PB", "{0} EB" };
            int i = 0;
            while (i < format.Length && s >= 1024) {
                s = (long)(100 * s / 1024) / 100.0;
                i++;
            }

            return string.Format(format[i], s);
        }

    }
}
