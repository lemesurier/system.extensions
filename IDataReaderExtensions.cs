﻿using System;
using System.Data;

namespace System.Data {
    public static class IDataReaderExtensions {
        public static bool ContainsField(this IDataReader r, string name) {
            if (r == null || name.IsNullOrEmpty()) {
                return false;
            }
            return r.GetSchemaTable().Columns.Contains(name);
        }
        public static object GetValueOrDefault(this IDataRecord r, string name, object defaultValue) {
            return r.GetValueOrNull(name) ?? defaultValue;
        }
        public static object GetValueOrNull(this IDataRecord r, string name) {
            for (int i = 0; i < r.FieldCount; i++) {
                if (r.GetName(i).Equals(name, StringComparison.InvariantCultureIgnoreCase)) {

                    if (r.IsDBNull(i)) return null;

                    return r.GetValue(i);

                    // at least end because we found the column
                    //break;
                }
            }
            return null;
        }
        public static Guid? GetGuidOrNull(this IDataRecord r, string name) {
            for (int i = 0; i < r.FieldCount; i++) {
                if (r.GetName(i).Equals(name, StringComparison.InvariantCultureIgnoreCase)) {

                    if (r.IsDBNull(i)) return null;

                    return r.GetGuid(i);

                    // at least end because we found the column
                    //break;
                }
            }
            return null;
        }
        public static Guid GetGuid(this IDataRecord r, string name) {
            return r.GetGuid(r.GetOrdinal(name));
        }
        public static bool IsDBNull(this IDataRecord r, string name) {
            return r.IsDBNull(r.GetOrdinal(name));
        }
        public static Int32 GetInt32(this IDataRecord r, string name) {
            return r.GetInt32(r.GetOrdinal(name));
        }
        public static string GetString(this IDataRecord r, string name) {
            return r.GetString(r.GetOrdinal(name));
        }
        public static DateTime GetDateTime(this IDataRecord r, string name) {
            return r.GetDateTime(r.GetOrdinal(name));
        }
        public static Int32? GetInt32OrNull(this IDataRecord r, string name) {
            for (int i = 0; i < r.FieldCount; i++) {
                if (r.GetName(i).Equals(name, StringComparison.InvariantCultureIgnoreCase)) {

                    if (r.IsDBNull(i)) return null;

                    return r.GetInt32(i);

                    // at least end because we found the column
                    //break;
                }
            }
            return null;
        }
        public static string GetStringOrNull(this IDataRecord r, string name) {

            for (int i = 0; i < r.FieldCount; i++) {
                if (r.GetName(i).Equals(name, StringComparison.InvariantCultureIgnoreCase)) {

                    if (r.IsDBNull(i)) return null;

                    return r.GetString(i);

                    // at least end because we found the column
                    //break;
                }
            }
            return null;
        }
    }
}