﻿using System.Text;

namespace System.Collections.Text
{
	public static class StringBuilderExtensions
	{
		/// <summary>
		/// Apends the text if the condition is true.
		/// </summary>
		public static void AppendIf(this StringBuilder sb, bool condition, char text)
		{
			if (condition)
			{
				sb.Append(text);
			}
		}

		/// <summary>
		/// Apends the text if the condition is true.
		/// </summary>
		public static void AppendIf(this StringBuilder sb, bool condition, string text)
		{
			if (condition)
			{
				sb.Append(text);
			}
		}
	}
}
