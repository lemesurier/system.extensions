﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Collections {
    public static class IEnumerableExtensions {

        public static string Join(this IEnumerable enumerable, string separator) {
            return enumerable.Join(separator, true);
        }
        public static string Join(this IEnumerable enumerable, string separator, bool includeNulls) {
            StringBuilder builder = new StringBuilder();

            int i = 0;
            foreach (object obj in enumerable) {
                if (!includeNulls && obj == null) {
                    continue;
                }
                if (i > 0) {
                    builder.Append(separator ?? string.Empty);
                }
                builder.Append(obj.ToString());
                i++;

            }

            return builder.ToString();
        }

        /// <summary>
        /// Performs an action for each item in each collection.
        /// </summary>
        /// <typeparam name="TCommon">A supertype or interface implemented by each member of each collection.</typeparam>
        public static void ForEach<TCommon>(Action<TCommon> action, params IEnumerable[] collections) {
            foreach (var col in collections) {
                foreach (TCommon item in col) {
                    action(item);
                }
            }
        }

        /// <summary>
        /// Searches a collection for an item.
        /// </summary>
        /// <returns>The found value, or the default value for "T".</returns>
        public static T Find<T>(this IEnumerable collection, Predicate<T> condition) {
            T foundItem = default(T);

            foreach (T item in collection) {
                if (condition(item)) {
                    foundItem = item;
                    break;
                }
            }

            return foundItem;
        }

        /// <summary>
        /// Returns a strongly-typed IEnumerable wrapper for the <paramref name="collection"/>.
        /// </summary>
        /// <typeparam name="T">The type of element in the collection.</typeparam>
        /// <param name="collection">The collection.</param>
        /// <remarks>Useful for utilizing the extension methods in <see cref="Enumerable"/> against non-generic collections.</remarks>
        public static IEnumerable<T> ToEnumerable<T>(this IEnumerable collection) {
            foreach (T item in collection) {
                yield return item;
            }
        }

    }
}
