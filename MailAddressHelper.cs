﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;

namespace System.Net.Mail {
    public class MailAddressesParser {
        public static MailAddressCollection GetAddresses(string s) {
            MailAddressCollection addresses = new MailAddressCollection();
            if (s.IsNullOrWhiteSpace()) return addresses;
            // split on space/tab/line break
            foreach (string line in s.Split(@"\s+")) {
                if (line.IsNullOrWhiteSpace()) continue;
                //split on comma/semicolon
                foreach (string test in line.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries)) {
                    if (test.IsNullOrWhiteSpace()) continue;
                    try {
                        addresses.Add(new MailAddress(test.Trim().ToLower()));
                    } catch (Exception) { }
                }
            }
            return addresses;
        }
        public static MailAddressCollection GetAddresses(IEnumerable<string> c) {
            MailAddressCollection addresses = new MailAddressCollection();
            if (c == null) return addresses;
            foreach (string s in c) {
                foreach (MailAddress a in GetAddresses(s)) addresses.Add(a);
            }
            return addresses;
        }
    }
}