﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Threading;

namespace System.Net.Mail {
    public static class SmtpUtility {

        public static bool SendSync(string from, string recipients, string subject, string body) {
            return SendSync(null, from, recipients, subject, body, true);
        }

        public static bool SendSync(SmtpClient smtp, string from, string recipients, string subject, string body) {
            return SendSync(smtp, from, recipients, subject, body, true);
        }

        public static bool SendSync(SmtpClient smtp, string from, string recipients, string subject, string body, bool throwOnError) {
            return SendSync(smtp, new MailMessage(from, recipients, subject, body), throwOnError);
        }

        public static bool SendSync(SmtpClient smtp, MailAddress from, MailAddress replyTo, MailAddressCollection recipients, string subject, string body, bool throwOnError) {
            return SendSync(smtp, AssembleMessage(from, replyTo, recipients, subject, body), throwOnError);
        }

        public static bool SendSync(SmtpClient smtp, MailAddress from, MailAddress replyTo, MailAddress to, string subject, string body, bool throwOnError) {
            return SendSync(smtp, AssembleMessage(from, replyTo, to, subject, body), throwOnError);
        }

        public static bool SendSync(SmtpClient smtp, MailMessage message) {

            return SendSync(smtp, message, true);

        }
        public static bool SendSync(SmtpClient smtp, MailMessage message, bool throwOnError) {

            smtp = smtp ?? new SmtpClient();
            if (smtp.Port != 25) {
                smtp.EnableSsl = true;
            }
            if (throwOnError) {
                smtp.Send(message);
            } else {
                try {
                    smtp.Send(message);
                } catch (Exception) {
                    return false;
                }
            }

            return true;

        }

        public static bool Send(string from, string recipients, string subject, string body) {
            return Send(null, from, recipients, subject, body, true);
        }

        public static bool Send(string from, string recipients, string subject, string body, bool throwOnError) {
            return Send(null, from, recipients, subject, body, throwOnError);
        }

        public static bool Send(SmtpClient smtp, string from, string recipients, string subject, string body) {
            return Send(smtp, from, recipients, subject, body, true);
        }

        public static bool Send(SmtpClient smtp, string from, string recipients, string subject, string body, bool throwOnError) {
            return Send(smtp, new MailMessage(from, recipients, subject, body), throwOnError);
        }

        public static bool Send(SmtpClient smtp, MailAddressCollection recipients, string subject, string body, bool throwOnError) {
            return Send(smtp, null, null, recipients, subject, body, throwOnError);
        }

        public static bool Send(SmtpClient smtp, MailAddress to, string subject, string body, bool throwOnError) {
            return Send(smtp, null, null, to, subject, body, throwOnError);
        }

        public static bool Send(MailAddressCollection recipients, string subject, string body) {
            return Send(null, null, null, recipients, subject, body, true);
        }

        public static bool Send(MailAddress to, string subject, string body) {
            return Send(null, null, null, to, subject, body, true);
        }

        public static bool Send(MailAddressCollection recipients, string subject, string body, bool throwOnError) {
            return Send(null, null, null, recipients, subject, body, throwOnError);
        }

        public static bool Send(MailAddress to, string subject, string body, bool throwOnError) {
            return Send(null, null, null, to, subject, body, throwOnError);
        }

        public static bool Send(SmtpClient smtp, MailAddressCollection recipients, string subject, string body) {
            return Send(smtp, null, null, recipients, subject, body, true);
        }

        public static bool Send(SmtpClient smtp, MailAddress to, string subject, string body) {
            return Send(smtp, null, null, to, subject, body, true);
        }

        public static bool Send(MailAddress from, MailAddressCollection recipients, string subject, string body) {
            return Send(null, from, null, recipients, subject, body, true);
        }

        public static bool Send(MailAddress from, MailAddress to, string subject, string body) {
            return Send(null, from, null, to, subject, body, true);
        }

        public static bool Send(MailAddress from, MailAddressCollection recipients, string subject, string body, bool throwOnError) {
            return Send(null, from, null, recipients, subject, body, throwOnError);
        }

        public static bool Send(MailAddress from, MailAddress to, string subject, string body, bool throwOnError) {
            return Send(null, from, null, to, subject, body, throwOnError);
        }

        public static bool Send(SmtpClient smtp, MailAddress from, MailAddressCollection recipients, string subject, string body) {
            return Send(smtp, from, null, recipients, subject, body, true);
        }

        public static bool Send(SmtpClient smtp, MailAddress from, MailAddress to, string subject, string body) {
            return Send(smtp, from, null, to, subject, body, true);
        }

        public static bool Send(SmtpClient smtp, MailAddress from, MailAddressCollection recipients, string subject, string body, bool throwOnError) {
            return Send(smtp, from, null, recipients, subject, body, throwOnError);
        }

        public static bool Send(SmtpClient smtp, MailAddress from, MailAddress to, string subject, string body, bool throwOnError) {
            return Send(smtp, from, null, to, subject, body, throwOnError);
        }

        public static bool Send(SmtpClient smtp, MailAddress from, MailAddress replyTo, MailAddressCollection recipients, string subject, string body) {
            return Send(smtp, from, replyTo, recipients, subject, body, true);
        }

        public static bool Send(SmtpClient smtp, MailAddress from, MailAddress replyTo, MailAddress to, string subject, string body) {
            return Send(smtp, from, replyTo, to, subject, body, true);
        }

        public static bool Send(SmtpClient smtp, MailAddress from, MailAddress replyTo, MailAddressCollection recipients, string subject, string body, bool throwOnError) {
            return Send(smtp, AssembleMessage(from, replyTo, recipients, subject, body), throwOnError);
        }

        public static bool Send(SmtpClient smtp, MailAddress from, MailAddress replyTo, MailAddress to, string subject, string body, bool throwOnError) {
            return Send(smtp, AssembleMessage(from, replyTo, to, subject, body), throwOnError);
        }

        public static bool Send(MailMessage message) {
            return Send(message, true);
        }

        public static bool Send(MailMessage message, bool throwOnError) {
            return Send(null, message, throwOnError);
        }

        public static bool Send(SmtpClient smtp, MailMessage message) {
            return Send(smtp, message, true);
        }

        // this is the core one
        public static bool Send(SmtpClient smtp, MailMessage message, bool throwOnError) {
            return SendAsync(smtp, message, throwOnError);
        }

        public static bool SendAsync(SmtpClient smtp, string from, string recipients, string subject, string body) {
            return SendAsync(smtp, from, recipients, subject, body, true);
        }

        public static bool SendAsync(SmtpClient smtp, string from, string recipients, string subject, string body, bool throwOnError) {
            return SendAsync(smtp, new MailMessage(from, recipients, subject, body), throwOnError);
        }

        public static bool SendAsync(SmtpClient smtp, MailAddress from, MailAddress replyTo, MailAddressCollection recipients, string subject, string body, bool throwOnError) {
            return SendAsync(smtp, AssembleMessage(from, replyTo, recipients, subject, body), throwOnError);
        }

        public static bool SendAsync(SmtpClient smtp, MailAddress from, MailAddress replyTo, MailAddress to, string subject, string body, bool throwOnError) {
            return SendAsync(smtp, AssembleMessage(from, replyTo, to, subject, body), throwOnError);
        }

        public static bool SendAsync(SmtpClient smtp, MailMessage message) {
            return SendAsync(smtp, message, true);
        }

        public static bool SendAsync(SmtpClient smtp, MailMessage message, bool throwOnError) {

            smtp = smtp ?? new SmtpClient();
            if (smtp.Port != 25) {
                smtp.EnableSsl = true;
            }
            if (throwOnError) {
                using (new SynchronizationContextSwitcher()) {
                    smtp.SendAsync(message, null);
                }
            } else {
                try {
                    using (new SynchronizationContextSwitcher()) {
                    smtp.SendAsync(message, null);
                }
                } catch (Exception) {
                    return false;
                }
            }

            return true;

        }

        private static MailMessage AssembleMessage(MailAddress from, MailAddress replyTo, MailAddressCollection to, string subject, string body) {
            MailMessage message = new MailMessage {
                Subject = subject,
                Body = body
            };

            if (from != null) message.From = from;
            if (replyTo != null) message.ReplyToList.Add(replyTo);

            foreach (MailAddress a in to) message.To.Add(a);

            return message;
        }

        private static MailMessage AssembleMessage(MailAddress from, MailAddress replyTo, MailAddress to, string subject, string body) {
            MailMessage message = new MailMessage {
                Subject = subject,
                Body = body
            };

            if (from != null) message.From = from;
            if (replyTo != null) message.ReplyToList.Add(replyTo);

            message.To.Add(to);

            return message;
        }
    }

    public static class SmptUtilityMailMessageExtensions {

        // extension methods
        public static bool Send(this MailMessage message) {
            return SmtpUtility.SendAsync(null, message, true);
        }

        public static bool Send(this MailMessage message, bool throwOnError) {
            return SmtpUtility.SendAsync(null, message, throwOnError);
        }

        public static bool Send(this MailMessage message, SmtpClient smtp) {
            return SmtpUtility.SendAsync(smtp, message, true);
        }

        public static bool Send(this MailMessage message, SmtpClient smtp, bool throwOnError) {
            return SmtpUtility.SendAsync(smtp, message, throwOnError);
        }

        public static bool SendAsync(this MailMessage message) {
            return SmtpUtility.SendAsync(null, message, true);
        }

        public static bool SendAsync(this MailMessage message, SmtpClient smtp) {
            return SmtpUtility.SendAsync(smtp, message, true);
        }

        public static bool SendAsync(this MailMessage message, SmtpClient smtp, bool throwOnError) {
            return SmtpUtility.SendAsync(smtp, message, throwOnError);
        }

        public static bool SendSync(this MailMessage message) {
            return SmtpUtility.SendSync(null, message, true);
        }

        public static bool SendSync(this MailMessage message, SmtpClient smtp) {
            return SmtpUtility.SendSync(smtp, message, true);
        }

        public static bool SendSync(this MailMessage message, SmtpClient smtp, bool throwOnError) {
            return SmtpUtility.SendSync(smtp, message, throwOnError);
        }

        /*public static bool SendBcc(this MailMessage message, MailAddress bccTo) {
            return message.SendBcc(null, bccTo, false);
        }

        public static bool SendBcc(this MailMessage message, SmtpClient smtp, MailAddress bccTo) {
            return message.SendBcc(smtp, bccTo, false);
        }

        public static bool SendBcc(this MailMessage message, SmtpClient smtp, MailAddress bccTo, bool throwOnError) {


            bool success = message.SendSync(smtp, throwOnError);

            if (bccTo != null) {
                message.To.Clear();
                message.To.Add(bccTo);
                message.SendSync(smtp, throwOnError);
            }

            return success;
        }*/

    }
}