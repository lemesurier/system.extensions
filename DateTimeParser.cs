﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System {
    public class DateTimeParser {
        public static DateTime ParseUtc(object date) {
            return ParseUtc(date.ToString());
        }
        public static DateTime ParseUtc(string date) {
            return DateTime.Parse(date, null, System.Globalization.DateTimeStyles.AssumeUniversal | System.Globalization.DateTimeStyles.AdjustToUniversal);
        }
        public static DateTime? ParseUtcOrDefault(object date, DateTime? defaultValue) {
            return ParseUtcOrDefault(date.ToString(), defaultValue);
        }
        public static DateTime? ParseUtcOrDefault(string date, DateTime? defaultValue) {
            DateTime newValue;
            return DateTime.TryParse(date, null, System.Globalization.DateTimeStyles.AssumeUniversal | System.Globalization.DateTimeStyles.AdjustToUniversal, out newValue) ? newValue : defaultValue;
        }
        public static DateTime? ParseUtcOrNull(object date) {
            if(date == null) return null;
            return ParseUtcOrNull(date.ToString());
        }
        public static DateTime? ParseUtcOrNull(string date) {
            return ParseUtcOrDefault(date, null);
        }
    }
}
