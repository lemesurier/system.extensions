﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace System.Security.Cryptography {
    public static class MD5Extensions {

        public static string ComputeHash(this System.Security.Cryptography.MD5 md5, string input) {

            byte[] bs = System.Text.Encoding.UTF8.GetBytes(input);
            bs = md5.ComputeHash(bs);
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            foreach (byte b in bs) {
                s.Append(b.ToString("x2").ToLower());
            }
            return s.ToString();

        }

    }

}

namespace System {

    public static class StringArrayExtensions {

        public static string Join(this string[] array, string separator) {

            return String.Join(separator, array);

        }

    }

    public static class GuidExtensions {
        public static bool IsEmpty(this Guid g) {
            return Guid.Empty.Equals(g);
        }
    }

    
}
