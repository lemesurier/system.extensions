﻿using System.Collections.Generic;

namespace System.Collections.Generic
{
	public static class GenericIListExtensions
	{
		/// <summary>
		/// Returns the first item in a generic IList.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list"></param>
		/// <returns>The first item in the list or the default value of T.</returns>
		public static T First<T>(this IList<T> list) 
		{
			if (list.Count == 0)
			{
				return default(T);
			}
			else
			{
				return list[0];
			}
		}

		/// <summary>
		/// Returns the last item in a generic IList.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list"></param>
		/// <returns>The last item in the list or the default value of T.</returns>
		public static T Last<T>(this IList<T> list) 
		{
			if (list.Count == 0)
			{
				return default(T);
			}
			else
			{
				return list[list.Count - 1];
			}
		}
	}
}
